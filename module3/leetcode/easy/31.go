package main

import (
	"sort"
)

func sortPeople(names []string, heights []int) []string {
	var res []string
	m := make(map[int]string)
	for i, height := range heights {
		m[height] = names[i]
	}
	sort.Ints(heights)
	for i := len(heights) - 1; i >= 0; i-- {
		res = append(res, m[heights[i]])
	}
	return res
}
