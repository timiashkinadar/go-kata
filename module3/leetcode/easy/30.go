package main

import (
	"math"
)

func countGoodTriplets(arr []int, a int, b int, c int) int {
	var res int
	for i := 0; i < len(arr)-2; i++ {
		for j := i + 1; j < len(arr)-1; j++ {
			if int(math.Abs(float64(arr[i])-float64(arr[j]))) <= a {
				for k := j + 1; k < len(arr); k++ {
					if int(math.Abs(float64(arr[j])-float64(arr[k]))) <= b &&
						int(math.Abs(float64(arr[i])-float64(arr[k]))) <= c {
						res++
					}
				}
			}

		}
	}
	return res
}
