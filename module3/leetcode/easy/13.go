package main

func numJewelsInStones(jewels string, stones string) int {
	a := 0
	for _, c := range jewels {
		for _, s := range stones {
			if c == s {
				a++
			}
		}
	}
	return a
}
