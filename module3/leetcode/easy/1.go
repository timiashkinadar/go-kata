package main

func tribonacci(n int) int {
	if n < 2 {
		return n
	} else if n == 2 {
		return 1
	}
	var prev1, prev, curr = 0, 1, 1
	for i := 3; i <= n; i++ {
		t := curr
		curr = curr + prev + prev1
		prev1 = prev
		prev = t
	}
	return curr
}
