package main

func smallerNumbersThanCurrent(nums []int) []int {
	var res []int
	for i := range nums {
		k := 0
		for y := range nums {
			if nums[i] > nums[y] && i != y {
				k++
			}
		}
		res = append(res, k)
	}
	return res
}
