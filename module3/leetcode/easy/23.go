package main

func interpret(command string) string {
	str := ""
	i := 0
	for i < len(command) {
		if command[i] == 'G' {
			str += "G"
			i++
		} else if command[i] == '(' && command[i+1] == ')' {
			str += "o"
			i = i + 2
		} else if command[i] == '(' && command[i+1] == 'a' && command[i+2] == 'l' && command[i+3] == ')' {
			str += "al"
			i = i + 4
		}
	}
	return str
}
