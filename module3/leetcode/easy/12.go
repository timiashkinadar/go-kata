package main

func numIdenticalPairs(nums []int) int {
	s := 0
	for i := range nums {
		for j := range nums {
			if nums[i] == nums[j] && i < j {
				s++
			}
		}
	}
	return s
}
