package main

func decompressRLElist(nums []int) []int {
	var res []int
	i := 0
	for i < len(nums)-1 {
		freg := nums[i]
		val := nums[i+1]
		for j := 0; j < freg; j++ {
			res = append(res, val)
		}
		i = i + 2
	}
	return res
}
