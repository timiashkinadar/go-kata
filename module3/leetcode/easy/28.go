package main

func countDigits(num int) int {
	number := num
	c := 0
	for num > 0 {
		d := num % 10
		if number%d == 0 {
			c++
		}
		num = num / 10
	}
	return c
}
