package main

func runningSum(nums []int) []int {
	var res []int
	v := 0
	for _, num := range nums {
		v += num
		res = append(res, v)
	}
	return res
}
