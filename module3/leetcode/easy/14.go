package main

import (
	"sort"
)

func maximumWealth(accounts [][]int) int {
	var res []int
	for _, m := range accounts {
		a := 0
		for _, s := range m {
			a += s
		}
		res = append(res, a)
	}
	sort.Ints(res)
	return res[len(res)-1]
}
