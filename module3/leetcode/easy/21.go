package main

func subtractProductAndSum(n int) int {
	prodDig := 1
	sumDig := 0
	for n > 0 {
		digit := n % 10
		sumDig += digit
		prodDig *= digit
		n = n / 10
	}
	return prodDig - sumDig
}
