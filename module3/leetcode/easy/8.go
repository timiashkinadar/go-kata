package main

func findKthPositive(arr []int, k int) int {
	var num, i = 1, 0
	for i < len(arr) {
		if arr[i] == num {
			i += 1
		} else if arr[i] > num {
			k -= 1
		}
		if k == 0 {
			return num
		}
		num += 1
	}
	return num + k - 1
}
