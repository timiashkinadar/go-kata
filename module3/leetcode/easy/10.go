package main

func shuffle(nums []int, n int) []int {
	var res []int
	j := n
	i := 0
	for i < n {
		res = append(res, nums[i])
		res = append(res, nums[j])
		i++
		j++
	}
	return res
}
