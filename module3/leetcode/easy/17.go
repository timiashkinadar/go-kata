package main

import (
	"strings"
)

func mostWordsFound(sentences []string) int {
	max := 0
	for _, sentence := range sentences {
		s := strings.Split(sentence, " ")
		if len(s) > max {
			max = len(s)
		}
	}
	return max
}
