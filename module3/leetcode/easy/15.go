package main

type ParkingSystem struct {
	b int
	m int
	s int
}

func Constructor(big int, medium int, small int) ParkingSystem {
	return ParkingSystem{
		b: big,
		m: medium,
		s: small,
	}
}

func (this *ParkingSystem) AddCar(carType int) bool {
	switch carType {
	case 1:
		if this.b >= 1 {
			this.b--
			return true
		} else {
			return false
		}
	case 2:
		if this.m >= 1 {
			this.m--
			return true
		} else {
			return false
		}
	case 3:
		if this.s >= 1 {
			this.s--
			return true
		} else {
			return false
		}
	}
	return false
}
