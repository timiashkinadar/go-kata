package main

import (
	"strings"
)

func finalValueAfterOperations(operations []string) int {
	x := 0
	for _, operation := range operations {
		if strings.HasPrefix(operation, "++") || strings.HasSuffix(operation, "++") {
			x++
		} else if strings.HasPrefix(operation, "--") || strings.HasSuffix(operation, "--") {
			x--
		}
	}
	return x
}
