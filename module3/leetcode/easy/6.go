package main

func uniqueMorseRepresentations(words []string) int {
	alphabet := []string{".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."}

	res := make(map[string]int)

	for _, word := range words {
		str := ""
		for _, letter := range word {
			str += alphabet[letter-'a']
		}
		res[str]++
	}

	return len(res)
}
