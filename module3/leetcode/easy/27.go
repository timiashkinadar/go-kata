package main

func balancedStringSplit(s string) int {
	i := 0
	n := 0
	for _, sym := range s {
		if sym == 'R' {
			i++
		} else if sym == 'L' {
			i--
		}
		if i == 0 {
			n++
		}
	}
	return n
}
