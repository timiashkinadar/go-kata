package main

func createTargetArray(nums []int, index []int) []int {
	var res = make([]int, len(nums))
	for y := 0; y < len(index); y++ {
		for i := len(res) - 1; i > index[y]; i-- {
			res[i] = res[i-1]
		}
		res[index[y]] = nums[y]
	}
	return res
}
