package main

func convertTemperature(celsius float64) []float64 {
	var res []float64
	res = append(res, celsius+273.15)
	res = append(res, celsius*1.80+32.00)
	return res
}
