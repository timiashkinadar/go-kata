package main

func differenceOfSum(nums []int) int {
	elSum := 0
	digitSum := 0
	for _, num := range nums {
		elSum += num
		if num > 9 {
			for num > 0 {
				d := num % 10
				digitSum += d
				num = num / 10
			}
		} else {
			digitSum += num
		}
	}
	return elSum - digitSum
}
