package main

func kidsWithCandies(candies []int, extraCandies int) []bool {
	max := 0
	var res []bool
	for _, candy := range candies {
		if candy > max {
			max = candy
		}
	}
	for _, candy := range candies {
		if candy+extraCandies >= max {
			res = append(res, true)
		} else {
			res = append(res, false)
		}
	}
	return res
}
