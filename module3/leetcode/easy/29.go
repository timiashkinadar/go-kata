package main

func xorOperation(n int, start int) int {
	var res int
	for i := 0; i < n; i++ {
		res ^= start
		start = start + 2
	}
	return res
}
