package main

//type TreeNode struct {
//	Val   int
//	Left  *TreeNode
//	Right *TreeNode
//}

func sumValues(node *TreeNode) (int, int, int) {
	if node == nil {
		return 0, 0, 0
	}

	leftSum, leftCount, resL := sumValues(node.Left)
	rightSum, rightCount, resR := sumValues(node.Right)

	totalSum := leftSum + rightSum + node.Val
	totalCount := leftCount + rightCount + 1
	res := resR + resL

	if totalSum/totalCount == node.Val {
		res++
	}
	return totalSum, totalCount, res
}

func averageOfSubtree(root *TreeNode) int {
	_, _, r := sumValues(root)
	return r
}

//func main() {
//	tree := &TreeNode{
//		Val: 4,
//		Left: &TreeNode{
//			Val: 8,
//			Left: &TreeNode{
//				Val:   0,
//				Left:  nil,
//				Right: nil,
//			},
//			Right: &TreeNode{
//				Val:   1,
//				Left:  nil,
//				Right: nil,
//			},
//		},
//		Right: &TreeNode{
//			Val:  5,
//			Left: nil,
//			Right: &TreeNode{
//				Val:   6,
//				Left:  nil,
//				Right: nil,
//			},
//		},
//	}
//	fmt.Println(averageOfSubtree(tree))
//}
