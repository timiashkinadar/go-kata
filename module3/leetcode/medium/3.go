package main

func mergeNodes(head *ListNode) *ListNode {
	newlist := &ListNode{}
	var newHead *ListNode
	newHead = newlist
	curr := head
	for curr.Next != nil {
		if curr.Val == 0 && curr != head {
			var n ListNode
			p := newlist.Next
			newlist.Next = &n
			n.Next = p
			newlist = newlist.Next
		} else if curr.Val != 0 {
			newlist.Val += curr.Val
		}
		curr = curr.Next
	}
	return newHead
}

//func main() {
//	listnode := &ListNode{
//		Val: 0,
//		Next: &ListNode{
//			Val: 3,
//			Next: &ListNode{
//				Val: 1,
//				Next: &ListNode{
//					Val: 0,
//					Next: &ListNode{
//						Val: 4,
//						Next: &ListNode{
//							Val: 5,
//							Next: &ListNode{
//								Val: 2,
//								Next: &ListNode{
//									Val:  0,
//									Next: nil,
//								},
//							},
//						},
//					},
//				},
//			},
//		},
//	}
//	fmt.Println(mergeNodes(listnode))
//}
