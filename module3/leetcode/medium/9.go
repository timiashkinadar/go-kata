package main

import (
	"sort"
)

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	var res []bool
	for i := 0; i < len(l); i++ {
		flag := false
		d1, d2 := l[i], r[i]
		var m []int
		for d1 <= d2 {
			m = append(m, nums[d1])
			d1++
		}
		sort.Ints(m)
		dif := m[0] - m[1]
		for i := 0; i < len(m)-1; i++ {
			if m[i]-m[i+1] != dif {
				res = append(res, false)
				flag = true
				break
			}
		}
		if flag != true {
			res = append(res, true)
		}
	}
	return res
}

//func main() {
//	r := checkArithmeticSubarrays([]int{-12, -9, -3, -12, -6, 15, 20, -25, -20, -15, -10}, []int{0, 1, 6, 4, 8, 7}, []int{4, 4, 9, 7, 9, 10})
//	fmt.Println(r)
//}
