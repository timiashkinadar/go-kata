package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func pairSum(head *ListNode) int {
	var s, sum []int

	for head != nil {
		s = append(s, head.Val)
		head = head.Next
	}
	l := len(s)
	last := l - 1

	for i := 0; i < l/2; i++ {
		sum = append(sum, s[i]+s[last])
		last--
	}

	max := sum[0]
	for _, i := range sum {
		if i > max {
			max = i
		}
	}
	return max
}

//func main() {
//	list1 := &ListNode{
//		Val: 4,
//		Next: &ListNode{
//			Val: 2,
//			Next: &ListNode{
//				Val: 2,
//				Next: &ListNode{
//					Val:  3,
//					Next: nil,
//				},
//			},
//		},
//	}
//	fmt.Println(pairSum(list1))
//}
