package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func deepestLeavesSum(root *TreeNode) int {
	var maxDepth, sum int
	var intersection func(*TreeNode, int)
	intersection = func(treeNode *TreeNode, depth int) {
		if treeNode == nil {
			return
		}
		if depth > maxDepth {
			maxDepth = depth
			sum = treeNode.Val
		} else if depth == maxDepth {
			sum += treeNode.Val
		}
		intersection(treeNode.Left, depth+1)
		intersection(treeNode.Right, depth+1)
	}
	intersection(root, 0)
	return sum
}

//func main() {
//	s := deepestLeavesSum(&TreeNode{
//		Val: 1,
//		Left: &TreeNode{
//			Val: 2,
//			Left: &TreeNode{
//				Val: 4,
//				Left: &TreeNode{
//					Val:   7,
//					Left:  nil,
//					Right: nil,
//				},
//				Right: nil,
//			},
//			Right: &TreeNode{
//				Val:   5,
//				Left:  nil,
//				Right: nil,
//			},
//		},
//		Right: &TreeNode{
//			Val:  3,
//			Left: nil,
//			Right: &TreeNode{
//				Val:  6,
//				Left: nil,
//				Right: &TreeNode{
//					Val:   8,
//					Left:  nil,
//					Right: nil,
//				},
//			},
//		},
//	})
//	fmt.Println(s)
//}
