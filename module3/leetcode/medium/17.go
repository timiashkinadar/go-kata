package main

func countPoints(points [][]int, queries [][]int) []int {
	var res = make([]int, len(queries))

	for i, query := range queries {
		x, y, r := query[0], query[1], query[2]
		c := 0
		for _, point := range points {
			x, y := point[0]-x, point[1]-y
			if x*x+y*y <= r*r {
				c++
			}
		}
		res[i] = c
	}
	return res
}

//func main() {
//	p := [][]int{
//		{1, 3},
//		{3, 3},
//		{5, 3},
//		{2, 2},
//	}
//
//	q := [][]int{
//		{2, 3, 1},
//		{4, 3, 1},
//		{1, 1, 2},
//	}
//	fmt.Println(countPoints(p, q))
//}
