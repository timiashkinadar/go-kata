package main

//type TreeNode struct {
//	Val   int
//	Left  *TreeNode
//	Right *TreeNode
//}

func constructMaximumBinaryTree(nums []int) *TreeNode {
	if len(nums) == 0 {
		return nil
	}
	maxIdx := maxElementIndex(nums)
	node := &TreeNode{Val: nums[maxIdx]}
	node.Left = constructMaximumBinaryTree(nums[:maxIdx])
	node.Right = constructMaximumBinaryTree(nums[maxIdx+1:])
	return node
}
func maxElementIndex(nums []int) int {
	max := nums[0]
	maxIdx := 0
	for i, num := range nums {
		if num > max {
			max = num
			maxIdx = i
		}
	}
	return maxIdx
}

//func main() {
//	fmt.Println(constructMaximumBinaryTree([]int{3, 2, 1, 6, 0, 5}))
//}
