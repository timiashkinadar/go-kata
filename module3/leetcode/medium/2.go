package main

import "sort"

func sortTheStudents(score [][]int, k int) [][]int {
	var res [][]int
	mapIndx := make(map[int][]int)
	var allSCores []int
	for _, ints := range score {
		allSCores = append(allSCores, ints[k])
		mapIndx[ints[k]] = ints
	}
	sort.Ints(allSCores)
	for i := len(allSCores) - 1; i >= 0; i-- {
		res = append(res, mapIndx[allSCores[i]])
	}
	return res
}
