package main

type SubrectangleQueries struct {
	r [][]int
}

func Constructor(rectangle [][]int) SubrectangleQueries {
	return SubrectangleQueries{r: rectangle}
}

func (this *SubrectangleQueries) UpdateSubrectangle(row1 int, col1 int, row2 int, col2 int, newValue int) {
	for i := row1; i <= row2; i++ {
		for j := col1; j <= col2; j++ {
			this.r[i][j] = newValue
		}
	}
}

func (this *SubrectangleQueries) GetValue(row int, col int) int {
	return this.r[row][col]
}

//func main() {
//	r := [][]int{
//		{1, 2, 1},
//		{4, 3, 4},
//		{3, 2, 1},
//		{1, 1, 1},
//	}
//	s := Constructor(r)
//	s.UpdateSubrectangle(0, 0, 3, 2, 5)
//
//}
