package main

func numTilePossibilities(tiles string) int {
	letters := make(map[rune]int)
	for _, letter := range tiles {
		letters[letter]++
	}
	return backtrack(letters)
}
func backtrack(letters map[rune]int) int {
	count := 0
	for letter, frequency := range letters {
		if frequency == 0 {
			continue
		}
		count++
		letters[letter]--
		count += backtrack(letters)
		letters[letter]++
	}
	return count
}

//func main() {
//	fmt.Println(numTilePossibilities("AAABBC"))
//}
