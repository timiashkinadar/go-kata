package main

func findSmallestSetOfVertices(n int, edges [][]int) []int {
	d := make([]bool, n)
	var res []int
	for _, e := range edges {
		d[e[1]] = true
	}

	for i, v := range d {
		if v == false {
			res = append(res, i)
		}
	}

	return res
}

//func main() {
//	edges := [][]int{
//		{0, 1},
//		{0, 2},
//		{2, 5},
//		{3, 4},
//		{4, 2},
//	}
//	r := findSmallestSetOfVertices(6, edges)
//	fmt.Println(r)
//}
