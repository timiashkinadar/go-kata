package main

func processQueries(queries []int, m int) []int {
	var p, res []int
	for i := 1; i <= m; i++ {
		p = append(p, i)
	}
	for _, query := range queries {
		for i, v := range p {
			if query == v {
				res = append(res, i)
				num := v
				copy(p[1:], p[:i])
				p[0] = num
			}
		}
	}
	return res
}

//func main() {
//	l := processQueries([]int{4, 1, 2, 2}, 4)
//	fmt.Println(l)
//}
