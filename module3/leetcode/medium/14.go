package main

func xorQueries(arr []int, queries [][]int) []int {
	var res []int
	for _, query := range queries {
		n := arr[query[0]]
		for i := query[0] + 1; i <= query[1]; i++ {
			n ^= arr[i]
		}
		res = append(res, n)
	}
	return res
}

//func main() {
//	a := [][]int{
//		{0, 1},
//		{1, 2},
//		{0, 3},
//		{3, 3},
//	}
//	fmt.Println(xorQueries([]int{1, 3, 4, 8}, a))
//}
