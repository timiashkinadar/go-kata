package main

//type TreeNode struct {
//	Val   int
//	Left  *TreeNode
//	Right *TreeNode
//}

func bstToGst(root *TreeNode) *TreeNode {
	sum := 0
	traverse(root, &sum)
	return root
}
func traverse(root *TreeNode, sum *int) {
	if root == nil {
		return
	}
	traverse(root.Right, sum)
	root.Val += *sum
	*sum = root.Val
	traverse(root.Left, sum)
}

//func main() {
//	tree := &TreeNode{
//		Val: 4,
//		Left: &TreeNode{
//			Val: 8,
//			Left: &TreeNode{
//				Val:   0,
//				Left:  nil,
//				Right: nil,
//			},
//			Right: &TreeNode{
//				Val:   1,
//				Left:  nil,
//				Right: nil,
//			},
//		},
//		Right: &TreeNode{
//			Val:  5,
//			Left: nil,
//			Right: &TreeNode{
//				Val:   6,
//				Left:  nil,
//				Right: nil,
//			},
//		},
//	}
//	fmt.Println(bstToGst(tree))
//}
