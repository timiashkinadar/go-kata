package main

//type TreeNode struct {
//	Val   int
//	Left  *TreeNode
//	Right *TreeNode
//}

func removeLeafNodes(root *TreeNode, target int) *TreeNode {
	if root == nil {
		return nil
	}

	root.Left = removeLeafNodes(root.Left, target)
	root.Right = removeLeafNodes(root.Right, target)

	if root.Val == target && root.Left == nil && root.Right == nil {
		return nil
	}

	return root
}

//func main() {
//	tree := &TreeNode{
//		Val: 1,
//		Left: &TreeNode{
//			Val: 2,
//			Left: &TreeNode{
//				Val:   2,
//				Left:  nil,
//				Right: nil,
//			},
//			Right: nil,
//		},
//		Right: &TreeNode{
//			Val: 3,
//			Left: &TreeNode{
//				Val:   2,
//				Left:  nil,
//				Right: nil,
//			},
//			Right: &TreeNode{
//				Val:   4,
//				Left:  nil,
//				Right: nil,
//			},
//		},
//	}
//	fmt.Println(removeLeafNodes(tree, 2))
//}
