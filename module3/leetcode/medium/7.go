package main

//type TreeNode struct {
//	Val   int
//	Left  *TreeNode
//	Right *TreeNode
//}

func balancedTree(nodes []int, start int, end int) *TreeNode {
	if start > end {
		return nil
	}

	mid := (start + end) / 2
	root := &TreeNode{
		Val:   nodes[mid],
		Left:  nil,
		Right: nil,
	}
	root.Left = balancedTree(nodes, start, mid-1)
	root.Right = balancedTree(nodes, mid+1, end)
	return root
}

func sortTree(root *TreeNode) []int {
	if root == nil {
		return []int{}
	}
	leftNodes := sortTree(root.Left)
	rightNodes := sortTree(root.Right)
	return append(append(leftNodes, root.Val), rightNodes...)
}

func balanceBST(root *TreeNode) *TreeNode {
	sort := sortTree(root)
	return balancedTree(sort, 0, len(sort)-1)
}

//func main() {
//	tree := &TreeNode{
//		Val: 2,
//		Right: &TreeNode{
//			Val:   3,
//			Left:  nil,
//			Right: nil,
//		},
//		Left: &TreeNode{
//			Val:   1,
//			Left:  nil,
//			Right: nil,
//		},
//	}
//	fmt.Println(balanceBST(tree))
//}
