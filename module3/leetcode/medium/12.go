package main

//type ListNode struct {
//	Val  int
//	Next *ListNode
//}

func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {
	curr := list1
	var start, end *ListNode

	for i := 0; i <= b; i++ {
		if i == a-1 {
			start = curr
		}
		if i == b {
			end = curr
		}
		curr = curr.Next
	}
	start.Next = list2
	curr = list2
	for curr.Next != nil {
		curr = curr.Next
	}
	curr.Next = end.Next
	return list1
}

//func main() {
//	list1 := &ListNode{
//		Val: 1,
//		Next: &ListNode{
//			Val: 2,
//			Next: &ListNode{
//				Val: 3,
//				Next: &ListNode{
//					Val: 4,
//					Next: &ListNode{
//						Val:  5,
//						Next: nil,
//					},
//				},
//			},
//		},
//	}
//	list2 := &ListNode{
//		Val: 1000000,
//		Next: &ListNode{
//			Val: 1000001,
//			Next: &ListNode{
//				Val:  1000002,
//				Next: nil,
//			},
//		},
//	}
//	l := mergeInBetween(list1, 3, 4, list2)
//	for l.Next != nil {
//		fmt.Println(l.Val)
//		l = l.Next
//	}
//}
