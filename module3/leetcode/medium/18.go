package main

func groupThePeople(groupSizes []int) [][]int {
	var res [][]int
	var m = make(map[int][]int)
	for i, size := range groupSizes {
		m[size] = append(m[size], i)
		if len(m[size]) == size {
			res = append(res, m[size])
			m[size] = nil
		}
	}
	return res
}

//func main() {
//	fmt.Println(groupThePeople([]int{3, 3, 3, 3, 3, 1, 3}))
//}
