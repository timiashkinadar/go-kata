package service

import (
	"fmt"
	"github.com/brianvoe/gofakeit"
	"gitlab.com/timiashkinadar/go-kata/module3/clean_architecture/cli/model"
)

type TaskRepository interface {
	GetTasks() ([]model.Task, error)
	GetTask(id uint) (model.Task, error)
	CreateTask(task model.Task) error
	UpdateTask(task model.Task) error
	DeleteTask(id uint) error
	SaveTasks(tasks []model.Task) error
}

type TodoService struct {
	repository TaskRepository
}

func NewTodoService(repository TaskRepository) *TodoService {
	return &TodoService{repository: repository}
}

func (s *TodoService) ListTodos() ([]model.Todo, error) {
	tasks, err := s.repository.GetTasks()
	if err != nil {
		return nil, err
	}
	var models = make([]model.Todo, 0, len(tasks))

	for _, task := range tasks {
		models = append(models, model.Todo{
			ID:          task.ID,
			Title:       task.Title,
			Description: task.Description,
			Status:      task.Status,
		})
	}

	return models, nil
}

func (s *TodoService) CreateTodo(title string, description string) (uint, error) {
	id := uint(gofakeit.Int32())
	err := s.repository.CreateTask(model.Task{
		ID:          id,
		Title:       title,
		Description: description,
		Status:      "in_progress",
	})
	return id, err
}

func (s *TodoService) CompleteTodo(id uint) error {
	todos, err := s.ListTodos()
	if err != nil {
		return err
	}

	for _, todo := range todos {
		if todo.ID == id {
			err = s.repository.UpdateTask(model.Task{
				ID:          todo.ID,
				Title:       todo.Title,
				Description: todo.Description,
				Status:      "completed",
			})
			return err
		}
	}

	return fmt.Errorf("todo with id:%d not fount", id)
}

func (s *TodoService) RemoveTodo(id uint) error {
	todos, err := s.ListTodos()
	if err != nil {
		return err
	}

	for _, todo := range todos {
		if todo.ID == id {
			err := s.repository.DeleteTask(id)
			if err != nil {
				return err
			}
			return err
		}
	}

	return fmt.Errorf("todo with id:%d not fount", id)
}

func (s *TodoService) UpdateById(id uint, title string, description string) error {
	todos, err := s.ListTodos()
	if err != nil {
		return err
	}

	for _, todo := range todos {
		if todo.ID == id {
			todo.Title = title
			todo.Description = description
			err := s.repository.UpdateTask(model.Task(todo))
			if err != nil {
				return err
			}
			return nil
		}
	}

	return fmt.Errorf("todo with id:%d not fount", id)
}
