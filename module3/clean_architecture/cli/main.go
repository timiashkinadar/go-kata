package main

import (
	"fmt"
	"gitlab.com/timiashkinadar/go-kata/module3/clean_architecture/cli/controller"
	"gitlab.com/timiashkinadar/go-kata/module3/clean_architecture/cli/repo"
	"gitlab.com/timiashkinadar/go-kata/module3/clean_architecture/cli/service"
	"log"
	"os"
)

func printMenu() {
	fmt.Println("Menu:")
	fmt.Println("1. Show the task list.")
	fmt.Println("2. Add a task.")
	fmt.Println("3. Delete the task.")
	fmt.Println("4. Edit a task by id.")
	fmt.Println("5. Complete the task.")
	fmt.Println("6. Exit.")
	fmt.Println("Enter the function number from the menu:")
}

func main() {
	printMenu()
	var num int
	_, err := fmt.Fscanln(os.Stdin, &num)
	if err != nil {
		log.Println(err)
	}
	rep := repo.NewFileTaskRepository("todo_list.json")
	serv := service.NewTodoService(rep)
	todoController := controller.NewTodoController(serv)

	for num != 6 {
		switch num {
		case 1:
			err := todoController.ShowToDo()
			if err != nil {
				log.Println(err)
			}
		case 2:
			todoController.AddToDo()
		case 3:
			todoController.DeleteToDo()
		case 4:
			todoController.EditToDo()
		case 5:
			todoController.CompleteToDo()
		default:
			fmt.Println("!!!Please, enter the number from one to 6!!!")
		}
		fmt.Println("Enter the function number from the menu:")
		_, err := fmt.Fscanln(os.Stdin, &num)
		if err != nil {
			log.Println(err)
		}
	}
	todoController.ExitToDo()
}
