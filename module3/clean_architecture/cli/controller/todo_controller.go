package controller

import (
	"fmt"
	"gitlab.com/timiashkinadar/go-kata/module3/clean_architecture/cli/model"
	"log"
	"os"
)

type TodoService interface {
	ListTodos() ([]model.Todo, error)
	CreateTodo(title string, description string) (uint, error)
	CompleteTodo(id uint) error
	RemoveTodo(id uint) error
	UpdateById(id uint, title string, description string) error
}

type TodoController interface {
	EditToDo()
	ShowToDo() error
	DeleteToDo()
	CompleteToDo()
	ExitToDo()
	AddToDo()
}

type todoController struct {
	service TodoService
}

func NewTodoController(service TodoService) TodoController {
	return &todoController{service: service}
}

func (c *todoController) ShowToDo() error {
	todos, err := c.service.ListTodos()
	if err != nil {
		return fmt.Errorf("error: %s! Please add a new task", err)
	}
	for i, todo := range todos {
		fmt.Println(i+1, todo)
	}
	return nil
}

func (c *todoController) AddToDo() {
	var title string
	var description string
	fmt.Println("Write a title:")
	_, err := fmt.Fscanln(os.Stdin, &title)
	if err != nil {
		log.Println(err)
	}
	fmt.Println("Write a description:")
	_, err = fmt.Fscanln(os.Stdin, &description)
	if err != nil {
		log.Println(err)
	}
	todoId, err := c.service.CreateTodo(title, description)
	if err != nil {
		log.Println(err)
	}
	fmt.Printf("Task with the id:%d added%s", todoId, "\n")
}

func (c *todoController) DeleteToDo() {
	fmt.Println("Enter the id to delete the task:")
	var id uint
	_, err := fmt.Fscanln(os.Stdin, &id)
	if err != nil {
		log.Println(err)
	}
	err = c.service.RemoveTodo(id)
	if err != nil {
		log.Println(err)
	}
}

func (c *todoController) EditToDo() {
	fmt.Println("Enter the id to edit the task:")
	var id uint
	_, err := fmt.Fscanln(os.Stdin, &id)
	if err != nil {
		log.Println(err)
	}
	fmt.Println("Enter the title to edit the task:")
	var title string
	_, err = fmt.Fscanln(os.Stdin, &title)
	if err != nil {
		log.Println(err)
	}
	fmt.Println("Enter the description to edit the task:")
	var description string
	_, err = fmt.Fscanln(os.Stdin, &description)
	if err != nil {
		log.Println(err)
	}
	err = c.service.UpdateById(id, title, description)
	if err != nil {
		log.Println(err)
	}
}

func (c *todoController) CompleteToDo() {
	fmt.Println("Enter the task id to complete the task:")
	var id uint
	_, err := fmt.Fscanln(os.Stdin, &id)
	if err != nil {
		log.Println(err)
	}
	err = c.service.CompleteTodo(id)
	if err != nil {
		log.Println(err)
	}
}

func (c *todoController) ExitToDo() {
	fmt.Print("App exit!")
	os.Exit(0)
}
