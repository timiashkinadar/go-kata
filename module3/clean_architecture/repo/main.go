package main

import (
	"fmt"
	"gitlab.com/timiashkinadar/go-kata/module3/clean_architecture/repo/repository"
)

func main() {
	ur := repository.InitFile()
	err := ur.Save(repository.User{
		ID:   186,
		Name: "hklrjhkg",
	})
	if err != nil {
		fmt.Println(err)
	}
	r, err := ur.Find(4)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(r)
	m, err := ur.FindAll()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(m)
}
