package repository

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
)

var filename = "users.json"

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File io.ReadWriter
}

func NewUserRepository(file io.ReadWriter) *UserRepository {
	return &UserRepository{
		File: file,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func InitFile() *UserRepository {
	file, err := os.OpenFile(filename, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		fmt.Errorf("Error: %s", err)
		return nil
	}
	ur := NewUserRepository(file)
	return ur
}

// Загружать данные в конструкторе

func (r *UserRepository) Save(record interface{}) error {
	ur := InitFile()
	b, err := io.ReadAll(ur.File)
	if err != nil {
		fmt.Println(err)
	}
	var data []User
	json.Unmarshal(b, &data)
	for _, dat := range data {
		if dat.ID == record.(User).ID {
			return fmt.Errorf("User has already exist!")
		}
	}
	data = append(data, record.(User))
	os.Truncate(filename, 0)
	bytes, _ := json.Marshal(data)
	_, err = ur.File.Write(bytes)
	if err != nil {
		fmt.Println(err)
	}
	return nil
}

func (r *UserRepository) Find(id int) (interface{}, error) {
	ur := InitFile()
	var res interface{}
	b, err := io.ReadAll(ur.File)
	if err != nil {
		return res, fmt.Errorf("User is not found. Error: %s", err)
	}
	var data []User
	err = json.Unmarshal(b, &data)
	if err != nil {
		return nil, err
	}
	for _, datum := range data {
		if datum.ID == id {
			res = datum
		}
	}
	if res == nil {
		return res, fmt.Errorf("User is not found. Error: %s", res)
	}
	return res, nil
}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	ur := InitFile()
	var res []interface{}
	b, _ := io.ReadAll(ur.File)
	var data []User
	err := json.Unmarshal(b, &data)
	if err != nil {
		return nil, err
	}
	for _, datum := range data {
		res = append(res, datum)
	}
	if data == nil {
		return res, fmt.Errorf("Empty file")
	}
	return res, nil
}
