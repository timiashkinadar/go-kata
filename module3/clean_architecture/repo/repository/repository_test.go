package repository

import (
	"encoding/json"
	"github.com/brianvoe/gofakeit"
	"os"
	"testing"
)

var (
	userRep *UserRepository
	file    *os.File
	users   = []User{
		{
			ID:   1,
			Name: "Alexandr",
		},
		{
			ID:   2,
			Name: "Mikhail",
		},
		{
			ID:   3,
			Name: "Pavel",
		},
	}
)

func createRepository(users *[]User, fileName string) *UserRepository {
	file, _ = os.OpenFile(fileName, os.O_RDWR|os.O_CREATE, 0777)
	defer file.Close()
	userRep = NewUserRepository(file)
	bytes, _ := json.Marshal(users)
	_, _ = userRep.File.Write(bytes)

	return userRep
}

func TestUserRepository_Save(t *testing.T) {
	filename = gofakeit.BuzzWord()
	repo := createRepository(&users, filename)
	defer os.Remove(filename)
	newUser := User{
		Name: "Name",
		ID:   100,
	}
	repo.Save(newUser)
	if output, _ := repo.Find(100); output != newUser {
		t.Errorf("Output %q not equal to expected %q", output, newUser)
	}
}

func TestUserRepository_Find(t *testing.T) {
	filename = gofakeit.BuzzWord()
	repo := createRepository(&users, filename)
	defer os.Remove(filename)
	type testCase struct {
		u User
	}
	testCasesFind := []testCase{
		{u: users[1]},
		{u: users[0]},
	}
	for _, test := range testCasesFind {
		if output, _ := repo.Find(test.u.ID); output != test.u {
			t.Errorf("Output %q not equal to expected %q", output, test.u)
		}
	}
	file.Close()
}

func TestUserRepository_FindAll(t *testing.T) {
	filename = gofakeit.BuzzWord()
	repo := createRepository(&users, filename)
	defer os.Remove(filename)
	numOfUsers := len(users)
	if output, _ := repo.FindAll(); numOfUsers != len(output) {
		t.Errorf("Output %q not equal to expected %q", output, numOfUsers)
	}
}
