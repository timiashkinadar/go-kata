package test

import (
	"encoding/json"
	"fmt"
	"github.com/brianvoe/gofakeit"
	"github.com/stretchr/testify/assert"
	"gitlab.com/timiashkinadar/go-kata/module3/clean_architecture/service/model"
	"gitlab.com/timiashkinadar/go-kata/module3/clean_architecture/service/repo"
	"gitlab.com/timiashkinadar/go-kata/module3/clean_architecture/service/service"
	"os"
	"testing"
)

var (
	serv     service.TodoService
	rep      service.TaskRepository
	filepath string
	todos    = []model.Todo{
		{
			ID:          1,
			Title:       "Todo1",
			Description: "Description1",
			Status:      "in_progress",
		},
		{
			ID:          2,
			Title:       "Todo2",
			Description: "Description2",
			Status:      "in_progress",
		},
		{
			ID:          3,
			Title:       "Todo3",
			Description: "Description3",
			Status:      "in_progress",
		},
	}
)

func initService(t *testing.T) service.TodoService {
	filepath = gofakeit.BuzzWord()
	file, err := os.OpenFile(filepath, os.O_RDWR|os.O_CREATE, 0777)
	assert.NoError(t, err)

	defer func(file *os.File) {
		err := file.Close()
		assert.NoError(t, err)
	}(file)

	rep = repo.NewFileTaskRepository(filepath)
	serv = service.NewTodoService(rep)
	bytes, _ := json.Marshal(todos)
	_, _ = file.Write(bytes)

	return serv
}

func TestTodoService_CreateTodo(t *testing.T) {
	srvc := initService(t)
	defer func(name string) {
		err := os.Remove(name)
		assert.NoError(t, err)
	}(filepath)

	id, err := srvc.CreateTodo("Todo4", "Description4")
	assert.NoError(t, err)

	output, _ := rep.GetTask(id)
	assert.Equal(t, output.ID, id)
}

func TestTodoService_RemoveTodo(t *testing.T) {
	srvc := initService(t)
	defer func(name string) {
		err := os.Remove(name)
		assert.NoError(t, err)
	}(filepath)

	err := srvc.RemoveTodo(todos[0].ID)
	assert.NoError(t, err)

	_, err = rep.GetTask(todos[0].ID)
	assert.Error(t, err)
	assert.Equal(t, err.Error(), fmt.Sprintf("task with id:%d not fount", todos[0].ID))
}

func TestTodoService_CompleteTodo(t *testing.T) {
	srvc := initService(t)
	defer func(name string) {
		err := os.Remove(name)
		assert.NoError(t, err)
	}(filepath)

	err := srvc.CompleteTodo(tasks[0].ID)
	assert.NoError(t, err)

	task, err := rep.GetTask(todos[0].ID)
	assert.NoError(t, err)
	assert.Equal(t, "completed", task.Status)
}

func TestTodoService_ListTodo(t *testing.T) {
	srvc := initService(t)
	defer func(name string) {
		err := os.Remove(name)
		assert.NoError(t, err)
	}(filepath)

	listTodos, err := srvc.ListTodos()
	assert.NoError(t, err)
	assert.Equal(t, listTodos, todos)
}
