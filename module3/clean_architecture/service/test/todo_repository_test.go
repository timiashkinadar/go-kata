package test

import (
	"encoding/json"
	"github.com/brianvoe/gofakeit"
	"github.com/stretchr/testify/assert"
	"gitlab.com/timiashkinadar/go-kata/module3/clean_architecture/service/model"
	"gitlab.com/timiashkinadar/go-kata/module3/clean_architecture/service/repo"
	"gitlab.com/timiashkinadar/go-kata/module3/clean_architecture/service/service"
	"log"
	"os"
	"testing"
)

var (
	file  *os.File
	tasks = []model.Task{
		{
			ID:          1,
			Title:       "Task1",
			Description: "Description1",
			Status:      "in_progress",
		},
		{
			ID:          2,
			Title:       "Task2",
			Description: "Description2",
			Status:      "in_progress",
		},
		{
			ID:          3,
			Title:       "Task3",
			Description: "Description3",
			Status:      "in_progress",
		},
	}
)

func createRepository(tasks *[]model.Task, filePath string) service.TaskRepository {
	file, _ = os.OpenFile(filePath, os.O_RDWR|os.O_CREATE, 0777)
	defer func(file *os.File) {
		err := file.Close()
		log.Println(err)
	}(file)
	r := repo.NewFileTaskRepository(filePath)
	bytes, _ := json.Marshal(tasks)
	_, _ = file.Write(bytes)

	return r
}

func TestFileTaskRepository_CreateTask(t *testing.T) {
	filepath = gofakeit.BuzzWord()
	rep := createRepository(&tasks, filepath)
	defer func(name string) {
		err := os.Remove(name)
		assert.NoError(t, err)
	}(filepath)
	newTask := model.Task{
		ID:          4,
		Title:       "Task4",
		Description: "Description4",
		Status:      "in_progress",
	}
	err := rep.CreateTask(newTask)
	assert.NoError(t, err)
	output, _ := rep.GetTask(newTask.ID)
	assert.Equal(t, output, newTask)
}

func TestFileTaskRepository_GetTask(t *testing.T) {
	filepath = gofakeit.BuzzWord()
	rep := createRepository(&tasks, filepath)
	defer func(name string) {
		err := os.Remove(name)
		log.Println(err)
	}(filepath)
	type testCase struct {
		t model.Task
	}
	testCasesGetTask := []testCase{
		{t: tasks[1]},
		{t: tasks[0]},
	}
	for _, test := range testCasesGetTask {
		output, err := rep.GetTask(test.t.ID)
		assert.NoError(t, err)
		assert.Equal(t, output, test.t)
	}
}

func TestFileTaskRepository_GetTasks(t *testing.T) {
	filepath = gofakeit.BuzzWord()
	rep := createRepository(&tasks, filepath)
	defer func(name string) {
		err := os.Remove(name)
		assert.NoError(t, err)
	}(filepath)
	output, err := rep.GetTasks()
	assert.NoError(t, err)
	assert.Equal(t, output, tasks)
}

func TestFileTaskRepository_DeleteTask(t *testing.T) {
	filepath = gofakeit.BuzzWord()
	rep := createRepository(&tasks, filepath)
	defer func(name string) {
		err := os.Remove(name)
		log.Println(err)
	}(filepath)
	type testCase struct {
		t model.Task
	}
	testCasesDeleteTask := []testCase{
		{t: tasks[1]},
		{t: tasks[0]},
	}
	for _, test := range testCasesDeleteTask {
		err := rep.DeleteTask(test.t.ID)
		assert.NoError(t, err)
		_, err = rep.GetTask(test.t.ID)
		assert.Error(t, err)
	}
}

func TestFileTaskRepository_UpdateTask(t *testing.T) {
	filepath = gofakeit.BuzzWord()
	rep := createRepository(&tasks, filepath)
	defer func(name string) {
		err := os.Remove(name)
		log.Println(err)
	}(filepath)
	type testCase struct {
		t model.Task
	}
	testCasesUpdateTask := []testCase{
		{t: model.Task{
			ID:          1,
			Title:       "UpdatedTask",
			Description: "UpdatedDescription",
			Status:      "completed",
		}},
		{t: model.Task{
			ID:          2,
			Title:       "UpdatedTask2",
			Description: "UpdatedDescription2",
			Status:      "completed2",
		}},
	}
	for _, test := range testCasesUpdateTask {
		err := rep.UpdateTask(test.t)
		assert.NoError(t, err)
		output, err := rep.GetTask(test.t.ID)
		assert.NoError(t, err)
		assert.NotEqual(t, output, test)
	}
}

func TestFileTaskRepository_SaveTasks(t *testing.T) {
	filepath = gofakeit.BuzzWord()
	rep := createRepository(&tasks, filepath)
	defer func(name string) {
		err := os.Remove(name)
		log.Println(err)
	}(filepath)

	newTasks := []model.Task{
		{
			ID:          111,
			Title:       "2324",
			Description: "34234",
			Status:      "45464",
		},
		{
			ID:          11,
			Title:       "23",
			Description: "334",
			Status:      "4544",
		},
	}

	err := rep.SaveTasks(newTasks)
	assert.NoError(t, err)
	getTasks, err := rep.GetTasks()
	assert.NoError(t, err)
	assert.Equal(t, newTasks, getTasks)
}
