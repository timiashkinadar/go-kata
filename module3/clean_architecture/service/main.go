package main

import (
	"fmt"
	"gitlab.com/timiashkinadar/go-kata/module3/clean_architecture/service/repo"
	"gitlab.com/timiashkinadar/go-kata/module3/clean_architecture/service/service"
	"log"
)

func main() {
	repository := repo.NewFileTaskRepository("TODO.json")
	serv := service.NewTodoService(repository)
	todoId, err := serv.CreateTodo("TITLE", "DESCRIPTION")
	if err != nil {
		log.Println(err)
	}
	todos, err := serv.ListTodos()
	if err != nil {
		log.Println(err)
	}
	fmt.Println(todos)

	err = serv.CompleteTodo(todoId)
	if err != nil {
		log.Println(err)
	}

	todos, err = serv.ListTodos()
	if err != nil {
		log.Println(err)
	}
	fmt.Println(todos)

	err = serv.RemoveTodo(todoId)
	if err != nil {
		log.Println(err)
	}

	todos, err = serv.ListTodos()
	if err != nil {
		log.Println(err)
	}
	fmt.Println(todos)
}
