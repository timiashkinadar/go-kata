package repo

import (
	"encoding/json"
	"fmt"
	"gitlab.com/timiashkinadar/go-kata/module3/clean_architecture/service/model"
	"io"
	"log"
	"os"
)

// FileTaskRepository is a file-based implementation of TaskRepository
type FileTaskRepository struct {
	FilePath string
}

func NewFileTaskRepository(filepath string) *FileTaskRepository {
	return &FileTaskRepository{filepath}
}

func (repo *FileTaskRepository) SaveTasks(tasks []model.Task) error {
	file, err := os.OpenFile(repo.FilePath, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		return err
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Println(err)
		}
	}(file)

	err = os.Truncate(repo.FilePath, 0)
	if err != nil {
		return err
	}
	bytes, _ := json.Marshal(tasks)
	_, err = file.Write(bytes)
	if err != nil {
		return err
	}
	return nil
}

func (repo *FileTaskRepository) CreateTask(task model.Task) error {
	file, err := os.OpenFile(repo.FilePath, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		log.Println(err)
		return err
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Println(err)
		}
	}(file)

	b, err := io.ReadAll(file)
	if err != nil {
		return err
	}

	var tasks []model.Task
	_ = json.Unmarshal(b, &tasks)

	for _, t := range tasks {
		if t.ID == task.ID {
			return fmt.Errorf("task has already exist")
		}
	}
	tasks = append(tasks, task)
	err = os.Truncate(repo.FilePath, 0)
	if err != nil {
		return err
	}
	bytes, _ := json.Marshal(tasks)
	_, err = file.Write(bytes)
	if err != nil {
		return err
	}
	return nil
}

// GetTasks returns all tasks from the repository
func (repo *FileTaskRepository) GetTasks() ([]model.Task, error) {
	var tasks []model.Task

	file, err := os.OpenFile(repo.FilePath, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		return nil, err
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Println(err)
		}
	}(file)

	content, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(content, &tasks); err != nil {
		return nil, err
	}

	if tasks == nil {
		return tasks, fmt.Errorf("empty file")
	}

	return tasks, nil
}

// GetTask returns a single task by its ID
func (repo *FileTaskRepository) GetTask(id uint) (model.Task, error) {
	var task model.Task

	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}

	for _, t := range tasks {
		if t.ID == id {
			return t, nil
		}
	}

	return task, fmt.Errorf("task with id:%d not fount", id)
}

// UpdateTask updates an existing task in the repository
func (repo *FileTaskRepository) UpdateTask(task model.Task) error {

	_, err := repo.GetTask(task.ID)
	if err != nil {
		return err
	}

	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}

	for i, t := range tasks {
		if t.ID == task.ID {
			tasks[i] = task
			break
		}
	}

	err = repo.SaveTasks(tasks)
	if err != nil {
		return err
	}
	return nil
}

func (repo *FileTaskRepository) DeleteTask(id uint) error {
	_, err := repo.GetTask(id)
	if err != nil {
		return err
	}

	var res []model.Task
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}

	for _, t := range tasks {
		if t.ID != id {
			res = append(res, t)
		}
	}

	err = repo.SaveTasks(res)
	if err != nil {
		return err
	}
	return nil
}
