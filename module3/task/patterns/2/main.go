package main

import "fmt"

// AirConditioner is the interface that defines the methods for controlling the air conditioner
type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
}

type AirConditionerAdapter struct {
	airConditioner RealAirConditioner
}

func (a AirConditionerAdapter) TurnOn() {
	a.airConditioner.TurnOnConditioner()
}

func (a AirConditionerAdapter) TurnOff() {
	a.airConditioner.TurnOffConditioner()
}

func (a AirConditionerAdapter) SetTemperature(temp int) {
	a.airConditioner.SetConditionerTemperature(temp)
}

type AirConditionerProxy struct {
	adapter       *AirConditionerAdapter
	authenticated bool
}

// RealAirConditioner is the implementation of the air conditioner
type RealAirConditioner struct {
}

func (r RealAirConditioner) TurnOnConditioner() {
	fmt.Println("Turning on the air conditioner")
}

func (r RealAirConditioner) TurnOffConditioner() {
	fmt.Println("Turning off the air conditioner")
}

func (r RealAirConditioner) SetConditionerTemperature(temp int) {
	fmt.Printf("Setting air conditioner temperature to %d", temp)
}

func NewAirConditionerProxy(authenticated bool) *AirConditionerProxy {
	return &AirConditionerProxy{
		authenticated: authenticated,
		adapter: &AirConditionerAdapter{
			airConditioner: RealAirConditioner{},
		},
	}
}

func (p *AirConditionerProxy) TurnOn() {
	if p.authenticated == true {
		p.adapter.TurnOn()
	} else {
		fmt.Println("Access denied: authentication required to turn on the air conditioner")
	}
}

func (p *AirConditionerProxy) TurnOff() {
	if p.authenticated == true {
		p.adapter.TurnOff()
	} else {
		fmt.Println("Access denied: authentication required to turn off the air conditioner")
	}
}

func (p *AirConditionerProxy) SetTemperature(temp int) {
	if p.authenticated == true {
		p.adapter.SetTemperature(temp)
	} else {
		fmt.Println("Access denied: authentication required to set temperature of the air conditioner")
	}
}

func main() {
	airConditioner := NewAirConditionerProxy(false) // without auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	airConditioner = NewAirConditionerProxy(true) // with auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)
}
