package main

import (
	"fmt"
	"reflect"
)

type PricingStrategy interface {
	Calculate(order *Order) float64
}

type Order struct {
	name     string
	price    float64
	quantity int
}

type RegularPricing struct {
}

type SalePricing struct {
	discount float64
}

func (p *RegularPricing) Calculate(order *Order) float64 {
	return order.price * float64(order.quantity)
}

func (p *SalePricing) Calculate(order *Order) float64 {
	discount := order.price * float64(order.quantity) * (p.discount / 100)
	return order.price*float64(order.quantity) - discount
}

func main() {
	order := &Order{
		name:     "Order1",
		price:    100,
		quantity: 3,
	}

	redularP := &RegularPricing{}
	saleP20 := &SalePricing{discount: 20}
	saleP10 := &SalePricing{discount: 10}
	saleP5 := &SalePricing{discount: 5}

	regularPrice := redularP.Calculate(order)
	salePrice20 := saleP20.Calculate(order)
	salePrice10 := saleP10.Calculate(order)
	salePrice5 := saleP5.Calculate(order)
	fmt.Printf("Total cost with %v strategy: %2.f", reflect.TypeOf(redularP), regularPrice)
	fmt.Println("\n", "--------------------")
	fmt.Printf("Total cost with %v strategy: %2.f%s", reflect.TypeOf(saleP20), salePrice20, "\n")
	fmt.Printf("Total cost with %v strategy: %2.f%s", reflect.TypeOf(saleP10), salePrice10, "\n")
	fmt.Printf("Total cost with %v strategy: %2.f%s", reflect.TypeOf(saleP5), salePrice5, "\n")
}
