package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

// WeatherAPI is the interface that defines the methods for accessing weather information
type WeatherAPI interface {
	GetTemperature(location string) int
	GetHumidity(location string) int
	GetWindSpeed(location string) int
}

type WeatherResponse struct {
	Name string `json:"name"`
	Main Main   `json:"main"`
	Wind Wind   `json:"wind"`
}

type Main struct {
	Temp     float64 `json:"temp"`
	Humidity int     `json:"humidity"`
}

type Wind struct {
	Speed float64 `json:"speed"`
}

func Kelvin2Celsius(k float64) int {
	return int(k - 273.15)
}

// OpenWeatherAPI is the implementation of the weather API
type OpenWeatherAPI struct {
	apiKey string
	client http.Client
}

func (o *OpenWeatherAPI) weatherInfo(location string) *WeatherResponse {
	URL := fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s", location, o.apiKey)
	reps, err := o.client.Get(URL)
	if err != nil {
		log.Fatal(err)
	}

	b, err := io.ReadAll(reps.Body)
	if err != nil {
		log.Fatal(err)
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(reps.Body)

	var t *WeatherResponse
	err = json.Unmarshal(b, &t)
	if err != nil {
		log.Fatal(err)
	}
	return t
}

func (o *OpenWeatherAPI) GetTemperature(location string) int {
	// Make a request to the open weather API to retrieve temperature information
	// and return the result
	// ...
	t := o.weatherInfo(location)
	temp := Kelvin2Celsius(t.Main.Temp)
	return temp
}

func (o *OpenWeatherAPI) GetHumidity(location string) int {
	// Make a request to the open weather API to retrieve humidity information
	// and return the result
	// ...
	t := o.weatherInfo(location)
	return t.Main.Humidity
}

func (o *OpenWeatherAPI) GetWindSpeed(location string) int {
	// Make a request to the open weather API to retrieve wind speed information
	// and return the result
	// ...
	t := o.weatherInfo(location)
	windSpeed := int(t.Wind.Speed)
	return windSpeed
}

// WeatherFacade is the facade that provides a simplified interface to the weather API
type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func (w *WeatherFacade) GetWeatherInfo(location string) (int, int, int) {
	temperature := w.weatherAPI.GetTemperature(location)
	humidity := w.weatherAPI.GetHumidity(location)
	windSpeed := w.weatherAPI.GetWindSpeed(location)

	return temperature, humidity, windSpeed
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{apiKey: apiKey, client: http.Client{}},
	}
}

func main() {
	weatherFacade := NewWeatherFacade("52020f041691fe6a4fec803d3baa8440")
	cities := []string{"Москва", "Санкт-Петербург", "Казань", "Якутск"}

	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city+": %d\n", temperature)
		fmt.Printf("Humidity in "+city+": %d\n", humidity)
		fmt.Printf("Wind speed in "+city+": %d\n\n", windSpeed)
	}
}
