package main

import (
	"encoding/json"
	"os"
	"testing"
	"time"
)

var commits = []Commit{
	{
		Message: "FirstCommit",
		UUID:    "1",
		Date:    time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC),
	},
	{
		Message: "SecondCommit",
		UUID:    "2",
		Date:    time.Date(2002, time.November, 10, 23, 0, 0, 0, time.UTC),
	},
	{
		Message: "ThirdCommit",
		UUID:    "3",
		Date:    time.Date(2005, time.November, 10, 23, 0, 0, 0, time.UTC),
	},
	{
		Message: "FourthCommit",
		UUID:    "4",
		Date:    time.Date(2008, time.November, 10, 23, 0, 0, 0, time.UTC),
	},
	{
		Message: "FifthCommit",
		UUID:    "5",
		Date:    time.Date(1999, time.November, 10, 23, 0, 0, 0, time.UTC),
	},
}

var (
	dll DoubleLinkedList

	node1 = Node{
		data: &commits[0],
	}
	node2 = Node{
		data: &commits[1],
	}
	node3 = Node{
		data: &commits[2],
	}
)

// Создание связ. списка с тремя элементами
func initTest() {
	dll.head = &node1
	dll.tail = &node3
	dll.curr = &node1
	dll.tail.next = nil
	dll.len = 3
	node1.next = &node2
	node2.next = &node3
	node2.prev = &node1
	node3.prev = &node2
}

func TestDoubleLinkedList_Len(t *testing.T) {
	initTest()
	testCases := []struct {
		name                string
		insertElementsCount int
		len                 int
	}{
		{
			name:                "TestLen1",
			insertElementsCount: 2,
		},
		{
			name:                "TestLen2",
			insertElementsCount: 4,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			startLen := dll.len
			for i := 0; i < tc.insertElementsCount; i++ {
				dll.Insert(0, commits[0])
			}
			if dll.Len() != startLen+tc.insertElementsCount {
				t.Errorf("Len() = %v, Len %v", dll.Len(), startLen+tc.insertElementsCount)
			}
		})
	}
}

func TestDoubleLinkedList_Current(t *testing.T) {
	initTest()
	testCases := []struct {
		name string
		curr *Node
	}{
		{
			name: "TestCurrent",
			curr: dll.curr,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if got := dll.Current(); got != tc.curr {
				t.Errorf("RealRes = %v, TestRes %v", got, tc.curr)
			}
		})
	}
}

func TestDoubleLinkedList_Next(t *testing.T) {
	initTest()
	testCases := []struct {
		name       string
		callsCount int
	}{
		{
			name:       "NextTest",
			callsCount: 1,
		},
		{
			name:       "Nexttest1",
			callsCount: 2,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			for i := 0; i < tc.callsCount; i++ {
				dll.Next()
			}
			if got, _ := dll.Index(); got != tc.callsCount {
				t.Errorf("RealRes = %v, TestRes %v", got, tc.callsCount)
			}
		})
	}
}

func TestDoubleLinkedList_Prev(t *testing.T) {
	initTest()

	testCases := []struct {
		name  string
		prevN int
	}{
		{
			name:  "PrevTest",
			prevN: 1,
		},
		{
			name:  "PrevTest1",
			prevN: 2,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			want, _ := dll.Index()
			for i := 0; i < tc.prevN; i++ {
				dll.Next()
			}
			for i := 0; i < tc.prevN; i++ {
				dll.Prev()
			}
			if got, _ := dll.Index(); got != want {
				t.Errorf("RealRes = %v, TestRes %v", got, tc.prevN)
			}
		})
	}
}

func TestDoubleLinkedList_LoadData(t *testing.T) {
	list := DoubleLinkedList{}
	jsonFile, _ := json.Marshal(commits)

	err := os.WriteFile("tem_file_name.json", jsonFile, 0666)
	if err != nil {
		t.Errorf(err.Error())
	}

	want := len(commits)
	err = list.LoadData("tem_file_name.json")
	if err != nil {
		t.Errorf("error %v", err)
		return
	}

	if got := list.Len(); got != want {
		t.Errorf("Len() = %v, want %v", got, want)
	}

	_ = os.Remove("tem_file_name.json")
}

func TestDoubleLinkedList_Insert(t *testing.T) {
	initTest()
	testCases := []struct {
		name   string
		commit Commit
		n      int
	}{
		{
			name: "Inserttest1",
			commit: Commit{
				Message: "FirstTestCase",
				UUID:    "FirstTestCase",
			},
			n: 2,
		},
		{
			name: "Inserttest2",
			commit: Commit{
				Message: "SecondTestCase",
				UUID:    "SecondTestCase",
			},
			n: 1,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			dll.Insert(tc.n, tc.commit)
			if got := dll.Search(tc.commit.Message); *got.data != tc.commit {
				t.Errorf("RealRes = %v, TestRes %v", got.data, tc.commit)
			}
		})
	}

}

func TestDoubleLinkedList_Delete(t *testing.T) {
	testCases := []struct {
		name string
		n    int
		len  int
	}{
		{
			name: "DeleteTest1",
			n:    0,
		},
		{
			name: "DeleteTest2",
			n:    1,
		},
	}
	for _, tc := range testCases {
		initTest()

		t.Run(tc.name, func(t *testing.T) {
			wantLen := dll.Len() - 1
			dll.Delete(tc.n)
			if dll.Len() != wantLen {
				t.Errorf("PrevLen = %v, Len %v", wantLen, dll.len)
			}
		})
	}
}

func TestDoubleLinkedList_DeleteCurrent(t *testing.T) {
	initTest()
	testCases := []struct {
		name string
		curr *Node
	}{
		{
			name: "DeleteCurrentTest",
			curr: dll.curr,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			listLen := dll.len
			dll.DeleteCurrent()
			if el := dll.Search(tc.curr.data.Message); el != nil && listLen != dll.len+1 {
				t.Errorf("Element = %v, len = %v", el, dll.len)
			}
		})
	}
}

func TestDoubleLinkedList_Index(t *testing.T) {
	testCases := []struct {
		name           string
		nextCallsCount int
	}{
		{
			name:           "IndexTest1",
			nextCallsCount: 1,
		},
		{
			name:           "IndexTest2",
			nextCallsCount: 2,
		},
	}

	for _, tc := range testCases {
		initTest()

		t.Run(tc.name, func(t *testing.T) {
			currIndex, _ := dll.Index()
			wantIndex := currIndex + tc.nextCallsCount

			for i := 0; i < tc.nextCallsCount; i++ {
				dll.Next()
			}
			if gotIndex, _ := dll.Index(); gotIndex != wantIndex {
				t.Errorf("gotIndex = %v, wantIndex = %v", gotIndex, wantIndex)
			}
		})
	}
}

func TestDoubleLinkedList_Pop(t *testing.T) {
	testCases := []struct {
		name        string
		wantElement Commit
	}{
		{
			name:        "PopTest",
			wantElement: commits[0],
		},
		{
			name:        "PopTest1",
			wantElement: commits[1],
		},
	}

	for _, tc := range testCases {
		initTest()
		t.Run(tc.name, func(t *testing.T) {
			err := dll.Insert(dll.len-1, tc.wantElement)
			if err != nil {
				t.Errorf("insertErr: %v", err)
				return
			}
			if gotElement := dll.Pop(); *gotElement.data != tc.wantElement {
				t.Errorf("RealRes = %v, TestRes %v", gotElement.data, tc.wantElement)
			}
		})
	}
}

func TestDoubleLinkedList_Shift(t *testing.T) {
	initTest()
	testCases := []struct {
		name        string
		wantElement Commit
	}{
		{
			name:        "Shifttest1",
			wantElement: commits[0],
		},
		{
			name:        "Shifttest2",
			wantElement: commits[1],
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			dll.Insert(-1, tc.wantElement)
			if gotElement := dll.Shift(); *gotElement.data != tc.wantElement {
				t.Errorf("RealRes = %v, TestRes %v", gotElement.data, tc.wantElement)
			}
		})
	}
}

func TestDoubleLinkedList_Search(t *testing.T) {
	initTest()
	testCases := []struct {
		name        string
		wantElement *Node
	}{
		{
			name:        "SearchTest1",
			wantElement: dll.head,
		},
		{
			name:        "SearchTest2",
			wantElement: dll.head.next,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if gotElement := dll.Search(tc.wantElement.data.Message); gotElement.data != tc.wantElement.data {
				t.Errorf("RealRes = %v, TestRes %v", gotElement.data, tc.wantElement.data)
			}
		})
	}
}

func TestDoubleLinkedList_SearchUUID(t *testing.T) {
	initTest()
	testCases := []struct {
		name        string
		wantElement *Node
	}{
		{
			name:        "SearchUUIDTest1",
			wantElement: dll.head,
		},
		{
			name:        "SearchUUIDTest2",
			wantElement: dll.head.next,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if gotElement := dll.SearchUUID(tc.wantElement.data.UUID); gotElement.data != tc.wantElement.data {
				t.Errorf("RealRes = %v, TestRes %v", gotElement.data, tc.wantElement.data)
			}
		})
	}
}

func TestDoubleLinkedList_Reverse(t *testing.T) {
	initTest()
	testCases := []struct {
		name    string
		oldHead *Node
	}{
		{
			name:    "Test_Reverse",
			oldHead: dll.head,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if res := dll.Reverse(); res.tail != tc.oldHead {
				t.Errorf("NewTail = %v, OldHead %v", res.tail, tc.oldHead)
			}
		})
	}
}
