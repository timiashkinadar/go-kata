package main

import (
	"testing"
)

func BenchmarkDoubleLinkedList_Len(b *testing.B) {
	for i := 0; i < b.N; i++ {
		dll.Len()
	}
}

func BenchmarkDoubleLinkedList_Current(b *testing.B) {
	for i := 0; i < b.N; i++ {
		dll.Current()
	}
}

func BenchmarkDoubleLinkedList_Next(b *testing.B) {
	initTest()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		dll.Next()
	}
}

func BenchmarkDoubleLinkedList_Prev(b *testing.B) {
	initTest()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		dll.Prev()
	}
}

func BenchmarkDoubleLinkedList_LoadData(b *testing.B) {
	initTest()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		dll.LoadData("data.json")
	}
}

func BenchmarkDoubleLinkedList_Insert(b *testing.B) {
	for i := 0; i < b.N; i++ {
		dll.Insert(-1, commits[0])
	}
}

func BenchmarkDoubleLinkedList_Delete(b *testing.B) {
	initTest()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		dll.Delete(0)
	}
}

func BenchmarkDoubleLinkedList_DeleteCurrent(b *testing.B) {
	initTest()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		dll.DeleteCurrent()
	}
}

func BenchmarkDoubleLinkedList_Index(b *testing.B) {
	initTest()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		dll.Index()
	}
}

func BenchmarkDoubleLinkedList_Shift(b *testing.B) {
	initTest()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		dll.Shift()
	}
}

func BenchmarkDoubleLinkedList_Pop(b *testing.B) {
	initTest()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		dll.Pop()
	}
}

func BenchmarkDoubleLinkedList_Search(b *testing.B) {
	initTest()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		dll.Search(commits[0].Message)
	}
}

func BenchmarkDoubleLinkedList_SearchUUID(b *testing.B) {
	initTest()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		dll.SearchUUID(commits[0].UUID)
	}
}

func BenchmarkDoubleLinkedList_Reverse(b *testing.B) {
	initTest()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		dll.Reverse()
	}
}
