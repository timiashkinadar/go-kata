package main

import "fmt"

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
	Insert(n int, c Commit) error
	Delete(n int) error
	DeleteCurrent() error
	Index() (int, error)
	Pop() *Node
	Shift() *Node
	SearchUUID(uuID string) *Node
	Search(message string) *Node
	Reverse() *DoubleLinkedList
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	res := d.curr.next
	if res != nil {
		d.curr = d.curr.next
	}
	return res
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	res := d.curr.prev
	if res != nil {
		d.curr = d.curr.prev
	}
	return res
}

// Insert вставка элемента после n элемента

//Вставка в начало списка с индексом -1

func (d *DoubleLinkedList) Insert(n int, commit Commit) error {
	node := &Node{
		data: &commit,
	}
	if n > d.len && n < -1 {
		return fmt.Errorf("len - %d, n - %d", d.len, n)
	}
	idx, err := d.Index()
	if err != nil {
		return err
	}
	if d.len == 0 && n == -1 {
		d.head = node
		d.curr = node
		d.tail = node
		d.len++
	} else if d.len > 0 && n == -1 {
		node.next = d.head
		d.head.prev = node
		d.head = node
		d.len++
	} else {
		if idx > n {
			for i := 0; i < idx-n; i++ {
				d.Prev()
			}
		} else {
			for i := 0; i < n-idx; i++ {
				d.Next()
			}
		}
		p := d.curr.next
		d.curr.next = node
		node.next = p
		node.prev = d.curr
		if p != nil {
			p.prev = node
		} else {
			d.tail = node
		}
		d.len++
	}
	return err
}

// Delete удаление n элемента

func (d *DoubleLinkedList) Delete(n int) error {
	idx, err := d.Index()
	if err != nil {
		return err
	}
	if idx > n {
		for i := 0; i < idx-n; i++ {
			d.Prev()
		}
	} else {
		for i := 0; i < n-idx; i++ {
			d.Next()
		}
	}
	d.DeleteCurrent()
	return err
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.len == 1 {
		d.head = nil
		d.tail = nil
		d.curr = nil
		d.len--
		return nil
	}
	prev := d.curr.prev
	next := d.curr.next
	if prev != nil {
		prev.next = d.curr.next
		d.curr = d.curr.prev
		d.len--
	} else {
		d.head = d.curr.next
		d.head.prev = nil
		d.curr = d.curr.next
		d.len--
	}
	if next != nil {
		next.prev = d.curr.prev
	} else {
		d.tail = d.tail.prev
	}
	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	step := d.head
	i := 0
	for step != d.curr {
		step = step.next
		i++
	}
	return i, nil
}

// Операция shift
func (d *DoubleLinkedList) Shift() *Node {
	if d.len == 0 {
		fmt.Errorf("len - %d", d.len)
	} else if d.len == 1 {
		val := d.head.data
		node := &Node{
			data: val,
		}
		d.head = nil
		d.tail = nil
		d.curr = nil
		d.len--
		return node
	} else {
		if d.curr == d.head {
			d.curr = d.curr.next
		}
		val := d.head.data

		d.head = d.head.next
		d.head.prev = nil

		d.len--

		node := &Node{
			data: val,
		}
		return node
	}
	return nil
}

// pop операция pop
func (d *DoubleLinkedList) Pop() *Node {
	if d.len == 0 {
		fmt.Errorf("len - %d", d.len)
	} else if d.len == 1 {
		val := d.tail.data
		node := &Node{
			data: val,
		}
		d.head = nil
		d.tail = nil
		d.curr = nil
		d.len--
		return node
	} else {
		if d.curr == d.tail {
			d.curr = d.curr.prev
		}
		val := d.tail.data

		d.tail = d.tail.prev
		d.tail.next = nil

		d.len--

		node := &Node{
			data: val,
		}
		return node
	}
	return nil
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	node := d.head
	for node != nil && uuID != node.data.UUID {
		node = node.next
	}
	return node
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	node := d.head
	for node != nil && message != node.data.Message {
		node = node.next
	}
	return node
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	curr := d.head
	newTail := d.head
	var prev *Node
	var next *Node
	for curr != nil {
		next = curr.next
		curr.next = prev
		prev = curr
		curr = next
	}
	d.head = prev
	d.tail = newTail
	return d
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}
