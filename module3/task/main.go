package main

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/brianvoe/gofakeit"
)

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func GenerateJSON() {
	// Дополнительное задание написать генератор данных
	// используя библиотеку gofakeit
	var c []Commit

	for i := 0; i < 20; i++ {
		c = append(c, Commit{
			Message: gofakeit.Quote(),
			UUID:    gofakeit.UUID(),
			Date:    gofakeit.Date(),
		})
	}

	file, _ := json.Marshal(c)
	err := os.WriteFile("RandomData.json", file, 0666)
	if err != nil {
		fmt.Errorf("error: %s", err)
	}
}

func quicksort(c []Commit) []Commit {
	if len(c) < 2 {
		return c
	}

	left, right := 0, len(c)-1

	pivot := len(c) / 2

	c[pivot], c[right] = c[right], c[pivot]

	for i, _ := range c {
		if c[i].Date.Before(c[right].Date) {
			c[left], c[i] = c[i], c[left]
			left++
		}
	}

	c[left], c[right] = c[right], c[left]

	quicksort(c[:left])
	quicksort(c[left+1:])

	return c
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	commitArr := []Commit{}

	data, err := os.ReadFile(path)
	if err != nil {
		fmt.Errorf("Error :%s\n", err)
	}

	err = json.Unmarshal(data, &commitArr)
	if err != nil {
		fmt.Printf("Error %s\n", err)
	}
	c := quicksort(commitArr)

	for i := -1; i < len(c)-1; i++ {
		err := d.Insert(i, c[i+1])
		if err != nil {
			return err
		}
	}
	return err
}
