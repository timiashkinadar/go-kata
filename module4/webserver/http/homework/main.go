package main

import (
	"context"
	"github.com/go-chi/chi/v5"
	"github.com/sirupsen/logrus"
	"gitlab.com/timiashkinadar/go-kata/module4/webserver/http/homework/handlers"
	"gitlab.com/timiashkinadar/go-kata/module4/webserver/http/homework/repository"
	"gitlab.com/timiashkinadar/go-kata/module4/webserver/http/homework/service"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	logger := logrus.New()
	r := chi.NewRouter()

	rep := repository.NewUserRepository()
	ser := service.NewUserService(rep)
	hand := handlers.NewHandler(ser, logger)

	r.Use(handlers.NewLoggerMiddleware(logger))
	r.Mount("/", hand.Routes())

	server := &http.Server{
		Addr:    ":3000",
		Handler: r,
	}

	go func() {
		logger.Printf("Starting server on %s", server.Addr)
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			logger.Fatalf("ListenAndServe(): %s", err)
		}
	}()

	stopAppCh := make(chan struct{})
	go gracefulShutdown(server, stopAppCh)

	<-stopAppCh

}

func gracefulShutdown(srv *http.Server, stopAppCh chan struct{}) {
	sgnl := make(chan os.Signal)
	signal.Notify(sgnl, syscall.SIGINT, syscall.SIGTERM)

	log.Println("Captured signal: ", <-sgnl)
	log.Println("Gracefully shutting down server...")

	if err := srv.Shutdown(context.Background()); err != nil {
		log.Println("Can't shutdown main server: ", err.Error())
	}
	stopAppCh <- struct{}{}
}
