package handlers

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/timiashkinadar/go-kata/module4/webserver/http/homework/domain"
	"io"
	"net/http"
	"strconv"
)

type Handlers interface {
	Routes() chi.Router
}

type UserService interface {
	GetUsers() ([]domain.User, error)
	SaveUser(user domain.User) (int, error)
	GetUser(id int) (domain.User, error)
	GetFileNames() ([]byte, error)

	UploadFile(file io.Reader, fileName string) error
}

type handler struct {
	s      UserService
	logger *logrus.Logger
}

func NewHandler(service UserService, l *logrus.Logger) Handlers {
	return &handler{s: service, logger: l}
}

func (h *handler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Route("/", func(r chi.Router) {
		r.Get("/users", h.getUsers)
		r.Post("/users", h.addUser)
		r.Get("/", h.helloHandler)
		r.Get("/users/{id}", h.getUserById)
		r.Post("/public/filename.txt", h.getFileNames)
		r.Post("/upload", h.uploadFile)
	})

	return r
}

func (h *handler) helloHandler(resp http.ResponseWriter, _ *http.Request) {
	_, err := resp.Write([]byte(`{"message": "Hello"}`))
	if err != nil {
		http.Error(resp, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h *handler) getUsers(resp http.ResponseWriter, _ *http.Request) {
	us, err := h.s.GetUsers()
	b, _ := json.Marshal(us)
	_, err = resp.Write(b)
	if err != nil {
		http.Error(resp, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h *handler) addUser(resp http.ResponseWriter, req *http.Request) {
	body, err := io.ReadAll(req.Body)
	if err != nil {
		http.Error(resp, err.Error(), http.StatusInternalServerError)
		return
	}
	defer func() {
		err := req.Body.Close()
		if err != nil {
			h.logger.Error(err)
		}
	}()

	user := domain.User{}
	err = json.Unmarshal(body, &user)
	if err != nil {
		http.Error(resp, err.Error(), http.StatusBadRequest)
		return
	}
	_, err = h.s.SaveUser(user)
	if err != nil {
		http.Error(resp, err.Error(), http.StatusBadRequest)
		return
	}

	resp.WriteHeader(http.StatusCreated)
}

func (h *handler) getUserById(resp http.ResponseWriter, req *http.Request) {
	userIdStr := chi.URLParam(req, "id")
	userIdInt, err := strconv.Atoi(userIdStr)
	if err != nil {
		http.Error(resp, err.Error(), http.StatusBadRequest)
		return
	}

	user, err := h.s.GetUser(userIdInt)
	if err != nil {
		http.Error(resp, err.Error(), http.StatusNotFound)
		return
	}

	b, err := json.Marshal(user)
	if err != nil {
		http.Error(resp, err.Error(), http.StatusBadRequest)
		return
	}

	_, err = resp.Write(b)
	if err != nil {
		http.Error(resp, err.Error(), http.StatusInternalServerError)
	}
	return
}

func (h *handler) getFileNames(resp http.ResponseWriter, req *http.Request) {
	all, err := h.s.GetFileNames()
	if err != nil {
		http.Error(resp, "Failed to get file_names", http.StatusInternalServerError)
		return
	}
	_, err = resp.Write(all)
	if err != nil {
		http.Error(resp, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h *handler) uploadFile(resp http.ResponseWriter, req *http.Request) {
	file, handler, err := req.FormFile("file")
	if err != nil {
		http.Error(resp, "Failed to get file from request", http.StatusBadRequest)
		return
	}
	defer file.Close()

	err = h.s.UploadFile(file, handler.Filename)
	if err != nil {
		http.Error(resp, err.Error(), http.StatusInternalServerError)
		return
	}

	resp.WriteHeader(http.StatusOK)
	write, err := resp.Write([]byte("File uploaded successfully\n"))
	if err != nil {
		h.logger.Error(write)
	}
}
