package handlers

import (
	"github.com/sirupsen/logrus"
	"net/http"
	"time"
)

type responseLogger struct {
	http.ResponseWriter
	statusCode int
}

func (l *responseLogger) WriteHeader(code int) {
	l.statusCode = code
	l.ResponseWriter.WriteHeader(code)
}

func NewLoggerMiddleware(logger *logrus.Logger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			start := time.Now()
			lw := &responseLogger{ResponseWriter: w, statusCode: 200}

			logger.WithFields(logrus.Fields{
				"method": r.Method,
				"path":   r.URL.Path,
			}).Info("request")

			next.ServeHTTP(lw, r)

			logger.WithFields(logrus.Fields{
				"method":   r.Method,
				"path":     r.URL.Path,
				"status":   lw.statusCode,
				"duration": time.Since(start),
			}).Info("response")
		})
	}
}
