package repository

import (
	"fmt"
	"gitlab.com/timiashkinadar/go-kata/module4/webserver/http/homework/domain"
	"gitlab.com/timiashkinadar/go-kata/module4/webserver/http/homework/service"
	"io"
	"os"
)

const fileWithNames = "public/filenames.txt"

type UserRepository struct {
	users []domain.User
}

func NewUserRepository() service.Repository {
	var users = []domain.User{
		{1, "Vera"},
		{2, "Nadya"},
		{3, "Lyuba"},
	}

	return &UserRepository{
		users: users,
	}
}

func (r *UserRepository) GetUsers() ([]domain.User, error) {
	return r.users, nil
}

func (r *UserRepository) SaveUser(user domain.User) (int, error) {
	r.users = append(r.users, user)
	return user.ID, nil
}

func (r *UserRepository) GetUser(id int) (domain.User, error) {
	for _, user := range r.users {
		if user.ID == id {
			return user, nil
		}
	}

	return domain.User{}, fmt.Errorf("user with id %d not found", id)
}

func (r *UserRepository) GetFileNames() ([]byte, error) {
	fileNamesFile, err := os.OpenFile(fileWithNames, os.O_RDWR, 0666)
	if err != nil {
		return nil, fmt.Errorf("open %v error: %w", fileWithNames, err)
	}
	defer fileNamesFile.Close()

	b, err := io.ReadAll(fileNamesFile)
	if err != nil {
		return nil, fmt.Errorf("read %v error: %w", fileWithNames, err)
	}
	return b, nil
}

func (r *UserRepository) openFile(path string) (*os.File, error) {
	fileNamesFile, err := os.OpenFile(path, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0666)
	if err != nil {
		return nil, err
	}
	return fileNamesFile, nil
}

func (r *UserRepository) SaveFile(f io.Reader, fileName string) error {
	file, err := r.openFile(fmt.Sprintf("public/%s", fileName))
	if err != nil {
		return err
	}
	defer file.Close()

	if _, err := io.Copy(file, f); err != nil {
		return err
	}

	return nil
}

func (r *UserRepository) SaveFileName(fileName string) error {
	file, err := r.openFile(fileWithNames)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = fmt.Fprintf(file, fmt.Sprintf("%s\n", fileName))
	if err != nil {
		return err
	}

	return nil
}
