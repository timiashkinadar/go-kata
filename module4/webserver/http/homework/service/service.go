package service

import (
	"fmt"
	"gitlab.com/timiashkinadar/go-kata/module4/webserver/http/homework/domain"
	"gitlab.com/timiashkinadar/go-kata/module4/webserver/http/homework/handlers"
	"io"
)

type Repository interface {
	GetUsers() ([]domain.User, error)
	SaveUser(user domain.User) (int, error)
	GetUser(id int) (domain.User, error)
	GetFileNames() ([]byte, error)

	SaveFileName(fileName string) error
	SaveFile(file io.Reader, fileName string) error
}

type UserService struct {
	repo Repository
}

func NewUserService(repo Repository) handlers.UserService {
	return &UserService{repo}
}

func (s *UserService) GetUsers() ([]domain.User, error) {
	return s.repo.GetUsers()
}

func (s *UserService) SaveUser(user domain.User) (int, error) {
	_, err := s.repo.GetUser(user.ID)
	if err == nil {
		return 0, fmt.Errorf("user with id %d has already exist", user.ID)
	}

	return s.repo.SaveUser(user)
}

func (s *UserService) GetUser(id int) (domain.User, error) {
	return s.repo.GetUser(id)
}

func (s *UserService) GetFileNames() ([]byte, error) {
	return s.repo.GetFileNames()
}

func (s *UserService) UploadFile(file io.Reader, fileName string) error {
	err := s.repo.SaveFile(file, fileName)
	if err != nil {
		return fmt.Errorf("failed to save file")
	}

	err = s.repo.SaveFileName(fileName)
	if err != nil {
		return fmt.Errorf("failed to save filename to public/filenames.txt")
	}

	return nil
}
