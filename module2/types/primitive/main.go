package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 234
	fmt.Println(unsafe.Sizeof(n))
}
