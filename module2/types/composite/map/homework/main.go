package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/qax-os/excelize",
			Stars: 13900,
		},
		{
			Name:  "https://github.com/aquasecurity/trivy",
			Stars: 15600,
		},
		{
			Name:  "https://github.com/grpc/grpc-go",
			Stars: 17400,
		},
		{
			Name:  "https://github.com/go-kratos/kratos",
			Stars: 19700,
		},
		{
			Name:  "https://github.com/k3s-io/k3s",
			Stars: 21900,
		},
		{
			Name:  "https://github.com/hashicorp/consul",
			Stars: 25900,
		},
		{
			Name:  "https://github.com/elastic/beats",
			Stars: 11500,
		},
		{
			Name:  "https://github.com/TykTechnologies/tyk",
			Stars: 8000,
		},
		{
			Name:  "https://github.com/Z4nzu/hackingtool",
			Stars: 26500,
		},
		{
			Name:  "https://github.com/restic/restic",
			Stars: 19000,
		},
		{
			Name:  "https://github.com/buger/goreplay",
			Stars: 16700,
		},
		{
			Name:  "https://github.com/slimtoolkit/slim",
			Stars: 15900,
		},
	}

	m := make(map[string]Project)
	for _, value := range projects {
		m[value.Name] = value
	}

	for _, value := range m {
		fmt.Println(value)
	}
}
