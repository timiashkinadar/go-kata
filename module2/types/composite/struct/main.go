package main

import (
	"fmt"
	"unsafe"
)

type User struct {
	Age      int
	Name     string
	Wallet   Wallet
	Location Location
}

type Wallet struct {
	RUB uint64
	USD uint64
	BTC uint64
	ETH uint64
}

type Location struct {
	Address string
	City    string
	index   string
}

func main() {
	user := User{
		Age:  13,
		Name: "Alexsander",
	}
	fmt.Println(user)
	wallet := Wallet{
		RUB: 250000,
		USD: 3500,
		BTC: 1,
		ETH: 4,
	}
	fmt.Println(wallet)
	fmt.Println("wallet allocates", unsafe.Sizeof(wallet), "bytes")
	fmt.Println(user)
	user2 := User{
		Age:  34,
		Name: "Anton",
		Wallet: Wallet{
			RUB: 144000,
			USD: 8900,
			BTC: 55,
			ETH: 34,
		},
		Location: Location{
			Address: "Address",
			City:    "Москва",
			index:   "108836",
		},
	}
	fmt.Println(user2)
}
