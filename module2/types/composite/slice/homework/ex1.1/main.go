package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	s = AppendSecondSolution(s)
	fmt.Println(s)
}

func AppendSecondSolution(s []int) []int {
	s = append(s, 4)
	return s
}
