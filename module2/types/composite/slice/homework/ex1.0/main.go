package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	AppendFirstSolution(&s)
	fmt.Println(s)
}

func AppendFirstSolution(s *[]int) {
	*s = append(*s, 4)
}
