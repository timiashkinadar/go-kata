package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)
	test(n)
}

func test(r interface{}) {
	switch r.(*int) {
	case nil:
		fmt.Println("Success!")
	default:
		fmt.Println("Fail!")
	}
}
