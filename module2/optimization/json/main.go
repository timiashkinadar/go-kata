package main

import (
	"encoding/json"

	jsoniter "github.com/json-iterator/go"
)

const jsonData = `{
  		"sold": 30,
  		"string": 592,
  		"NotABC": 1,
  		"unavailable": 2,
  		"pending": 4,
  		"available": 364,
  		"peric": 1,
  		"totvs1": 1}`

func UnmarshalInventory(data []byte) (Inventory, error) {
	var r Inventory
	err := json.Unmarshal(data, &r)
	return r, err
}

func UnmarshalInventory2(data []byte) (Inventory, error) {
	var r Inventory
	err := jsoniter.Unmarshal(data, &r)
	return r, err
}

func (r *Inventory) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

func (r *Inventory) Marshal2() ([]byte, error) {
	return jsoniter.Marshal(r)
}

type Inventory struct {
	Sold        int64 `json:"sold"`
	String      int64 `json:"string"`
	NotABC      int64 `json:"NotABC"`
	Unavailable int64 `json:"unavailable"`
	Pending     int64 `json:"pending"`
	Available   int64 `json:"available"`
	Peric       int64 `json:"peric"`
	Totvs1      int64 `json:"totvs1"`
}
