package main

import (
	"testing"
)

var (
	inventory Inventory
	err       error
	data      []byte
)

func BenchmarkStandardJsonMarshal(b *testing.B) {
	for i := 0; i < b.N; i++ {
		data, err = inventory.Marshal()
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkStandardJsonUnmarshal(b *testing.B) {
	for i := 0; i < b.N; i++ {
		inventory, err = UnmarshalInventory([]byte(jsonData))
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkJsonIterMarshal(b *testing.B) {
	for i := 0; i < b.N; i++ {
		data, err = inventory.Marshal2()
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkJsonIterUnmarshal(b *testing.B) {
	for i := 0; i < b.N; i++ {
		inventory, err = UnmarshalInventory2([]byte(jsonData))
		if err != nil {
			panic(err)
		}
	}
}
