package main

import "fmt"

func Greet(name string) string {
	r := []rune(name)
	if r[0] >= 'A' && r[0] <= 'Z' {
		return fmt.Sprintf("Hello %s, you welcome!", name)
	} else if r[0] >= 'А' && r[0] <= 'Я' {
		return fmt.Sprintf("Привет %s, добро пожаловать!", name)
	} else {
		return ""
	}
}
