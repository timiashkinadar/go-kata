package main

import (
	"testing"
)

func BenchmarkSimplest(b *testing.B) {
	users := genUsers()
	products := genProducts()
	for i := 0; i < b.N; i++ {
		MapUserProducts(users, products)
	}
}

func BenchmarkSimplest2(b *testing.B) {
	users := genUsers()
	products := genProducts()
	for i := 0; i < b.N; i++ {
		MapUserProducts2(users, products)
	}
}
