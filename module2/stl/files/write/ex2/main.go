package main

import (
	"fmt"
	"io"
	"os"

	"github.com/alexsergivan/transliterator"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	openFile, err := os.Open("module2/stl/files/write/ex2/example.txt")
	check(err)
	defer openFile.Close()

	file, err := os.Create("module2/stl/files/write/ex2/example.processed.txt")
	defer file.Close()
	check(err)

	trans := transliterator.NewTransliterator(nil)

	var n int
	data := make([]byte, 100)
	for {
		n, err = openFile.Read(data)
		if err == io.EOF {
			break
		}
		text := trans.Transliterate(string(data[:n]), "en")
		file.WriteString(text)
	}
	fmt.Println("Transliterated file created!")
}
