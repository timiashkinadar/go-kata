package main

import (
	"bufio"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	fmt.Print("Enter your name: ")

	reader := bufio.NewReader(os.Stdin)
	name, _ := reader.ReadString('\n')

	file, err := os.Create("module2/stl/files/write/ex1/file")
	check(err)
	defer file.Close()

	_, err = file.WriteString(name)
	check(err)
	fmt.Println("File with name created!")
}
