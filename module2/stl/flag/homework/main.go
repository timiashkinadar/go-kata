package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

type Config struct {
	AppName    string `json:"app_name"`
	Production bool   `json:"production"`
}

func main() {
	var filePath *string
	filePath = flag.String("conf", "./config.json", "pass filename here")
	flag.Parse()

	conf := Config{}

	openFile, err := os.Open(*filePath)
	if err != nil {
		fmt.Errorf("Error :%s\n", err)
	}
	defer openFile.Close()

	d := make([]byte, 300)
	n1, err := openFile.Read(d)
	d = d[:n1]

	err = json.Unmarshal(d, &conf)
	if err != nil {
		fmt.Printf("Error %s\n", err)
	}
	fmt.Printf("%+v\n", conf)
}

// Пример вывода конфига
// Production: true
// AppName: мое тестовое приложение
