module gitlab.com/timiashkinadar/go-kata/module2/stl/utf8/homework

go 1.19

require (
	github.com/alexsergivan/transliterator v1.0.0
	github.com/makeworld-the-better-one/go-isemoji v1.3.0
)
