// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
	"io"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	file, err := os.Create("module2/stl/io/homework/example.txt")
	defer file.Close()
	check(err)

	for i, v := range data {
		if i != 0 {
			file.WriteString("\n")
		}
		file.WriteString(v)
	}

	openFile, err := os.Open("module2/stl/io/homework/example.txt")
	check(err)
	defer openFile.Close()

	d := make([]byte, 300)
	for {
		n1, err := openFile.Read(d)
		if err == io.EOF {
			break
		}
		fmt.Println(string(d[:n1]))
	}
}
