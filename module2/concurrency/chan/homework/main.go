package main

import (
	"fmt"
	"sync"
	"time"
)

func joinChannels(chs ...<-chan int) <-chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
		fmt.Println("Main channel just closed!")
	}()

	return mergedCh
}

func main() {

	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	thicker := time.NewTicker(time.Second * 30)

	go func() {
		for _, num := range []int{1, 2, 3} {
			a <- num
		}
	}()

	go func() {
		for _, num := range []int{20, 10, 30} {
			b <- num
		}
	}()

	go func() {
		for _, num := range []int{300, 200, 100} {
			c <- num
		}
	}()

	mainChan := joinChannels(a, b, c)

	for {
		select {
		case num, ok := <-mainChan:
			if !ok {
				return
			}
			fmt.Println(num)
		case <-thicker.C:
			fmt.Println("Time is over!")
			close(a)
			fmt.Println("Channel A just closed!")
			close(b)
			fmt.Println("Channel B just closed!")
			close(c)
			fmt.Println("Channel C just closed!")
		}
	}

}
