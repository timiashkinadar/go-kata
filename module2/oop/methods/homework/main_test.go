package main

import (
	"fmt"
	"testing"
)

func TestCalc_multiply(t *testing.T) {
	type testCase struct {
		a, b, res float64
	}
	testCasesMultiply := []testCase{
		{a: 2, b: 2, res: 4},
		{a: 1, b: 8, res: 8},
		{a: 9, b: 9, res: 81},
		{a: 6, b: 2, res: 12},
		{a: 9, b: 4, res: 36},
		{a: 5, b: 5, res: 25},
		{a: 7, b: 4, res: 28},
		{a: 5, b: 0, res: 0},
	}

	for i, test := range testCasesMultiply {
		t.Run(fmt.Sprintf("Multiply #%d", i), func(t *testing.T) {
			realRes := multiply(test.a, test.b)
			if realRes != test.res {
				t.Errorf("%f != %f", test.res, realRes)
			}
		})
	}

}

func TestCalc_sum(t *testing.T) {
	type testCase struct {
		a, b, res float64
	}

	testCasesSum := []testCase{
		{a: 2, b: 2, res: 4},
		{a: 1, b: 8, res: 9},
		{a: 9, b: 9, res: 18},
		{a: 6, b: 2, res: 8},
		{a: 9, b: 4, res: 13},
		{a: 5, b: 5, res: 10},
		{a: 7, b: 4, res: 11},
		{a: 5, b: 0, res: 5},
	}

	for i, test := range testCasesSum {
		t.Run(fmt.Sprintf("Sum #%d", i), func(t *testing.T) {
			realRes := sum(test.a, test.b)
			if realRes != test.res {
				t.Errorf("%f != %f", test.res, realRes)
			}
		})
	}
}

func TestCalc_divide(t *testing.T) {
	type testCase struct {
		a, b, res float64
	}

	testCasesDivide := []testCase{
		{a: 2, b: 2, res: 1},
		{a: 90, b: 9, res: 10},
		{a: 9, b: 9, res: 1},
		{a: 6, b: 2, res: 3},
		{a: 8, b: 4, res: 2},
		{a: 5, b: 5, res: 1},
		{a: 9, b: 3, res: 3},
		{a: 0, b: 5, res: 0},
	}

	for i, test := range testCasesDivide {
		t.Run(fmt.Sprintf("Divide #%d", i), func(t *testing.T) {
			realRes := divide(test.a, test.b)
			if realRes != test.res {
				t.Errorf("%f != %f", test.res, realRes)
			}
		})
	}
}

func TestCalc_average(t *testing.T) {
	type testCase struct {
		a, b, res float64
	}
	testCasesAverage := []testCase{
		{a: 2, b: 2, res: 2},
		{a: 90, b: 10, res: 50},
		{a: 9, b: 9, res: 9},
		{a: 6, b: 2, res: 4},
		{a: 8, b: 4, res: 6},
		{a: 5, b: 5, res: 5},
		{a: 9, b: 3, res: 6},
		{a: 0, b: 6, res: 3},
	}

	for i, test := range testCasesAverage {
		t.Run(fmt.Sprintf("Average #%d", i), func(t *testing.T) {
			realRes := average(test.a, test.b)
			if realRes != test.res {
				t.Errorf("%f != %f", test.res, realRes)
			}
		})
	}
}
